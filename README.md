# Modules 101

* [Slides and Code Examples](http://tokiotajs.bitbucket.org/modules101/viewer/)

## Examples

Check out the [Example Viewer](http://tokiotajs.bitbucket.org/modules101/viewer/). Use left and right arrow keys to navigate.

 * [Example 1](http://tokiotajs.bitbucket.org/modules101/viewer/#/1) - Monolithic
 * [Example 2](http://tokiotajs.bitbucket.org/modules101/viewer/#/2) - Global Variables
 * [Example 3](http://tokiotajs.bitbucket.org/modules101/viewer/#/3) - Namespace
 * [Example 4](http://tokiotajs.bitbucket.org/modules101/viewer/#/4) - Bundling with concatenation
 * [Example 5](http://tokiotajs.bitbucket.org/modules101/viewer/#/5) - Minification
 * [Example 6](http://tokiotajs.bitbucket.org/modules101/viewer/#/6) - CommonJS & Browserify
 * [Example 7](http://tokiotajs.bitbucket.org/modules101/viewer/#/7) - CommonJS & Webpack
 * [Example 8](http://tokiotajs.bitbucket.org/modules101/viewer/#/8) - AMD & Webpack
 * [Example 9](http://tokiotajs.bitbucket.org/modules101/viewer/#/9) - AMD & RequireJS
 * [Example 10](http://tokiotajs.bitbucket.org/modules101/viewer/#/10) - AMD & RequireJS Optimizer
 * [Example 11](http://tokiotajs.bitbucket.org/modules101/viewer/#/11) - Gulp & amd-optimize
 * [Example 12](http://tokiotajs.bitbucket.org/modules101/viewer/#/12) - Bundling Almond with Gulp
 * [Example 13](http://tokiotajs.bitbucket.org/modules101/viewer/#/13) - AMD & deamdify
 * [Example 14](http://tokiotajs.bitbucket.org/modules101/viewer/#/14) - ES6 & Babelify (Browserify)
 * [Example 15](http://tokiotajs.bitbucket.org/modules101/viewer/#/15) - ES6 & Babelify (Webpack)
 * [Example 16](http://tokiotajs.bitbucket.org/modules101/viewer/#/16) - ES6 & Babelify (RequireJs)
 * [Example 17](http://tokiotajs.bitbucket.org/modules101/viewer/#/17) - ES6 & es6ify (Browserify)
 * [Example 18](http://tokiotajs.bitbucket.org/modules101/viewer/#/18) - ES6 & Esperanto
 * [Example 19](http://tokiotajs.bitbucket.org/modules101/viewer/#/19) - ES6 & Rollup
 * [Example 20](http://tokiotajs.bitbucket.org/modules101/viewer/#/20) - ES6 & JSPM
 * [Example 21](http://tokiotajs.bitbucket.org/modules101/viewer/#/21) - Multi-format & JSPM




  ```
  ```
___
By [Sílvia Mur](https://twitter.com/pchiwan) & [Roberto Huertas](https://twitter.com/newton_w).   
June 2015

Feel free to use and modify this presentation framework for your own presentations! MIT License.  
Based on [Curran Kelleher's amazing work!](https://github.com/curran/portfolio)