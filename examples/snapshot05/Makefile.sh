#!/bin/bash

# clean generated files
rm -f main-bundle.js
rm -f main-bundle-min.js

# concatenate all js files into one
cat game.js enums.js breed.js class.js character.js > game-bundle.js

# uglify concatenated bundle file
# Depends on having uglifyjs command installed, with the command:
# npm install uglifyjs -g
uglifyjs game-bundle.js --compress --mangle --output game-bundle-min.js
