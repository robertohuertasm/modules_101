#!/bin/bash

# clean generated files
rm -f main-bundle.js
rm -f main-bundle-min.js

# Depends on having browserify command installed, with the command:
# npm install browserify -g
browserify main.js -o main-bundle.js

# Depends on having uglifyjs command installed, with the command:
# npm install uglifyjs -g
uglifyjs main-bundle.js --compress --mangle -o main-bundle-min.js