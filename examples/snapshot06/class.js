function Class(classname) {
	this.name = classname;
	this.toString = function () { return this.name; };
}

module.exports = Class;