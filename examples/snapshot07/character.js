var Breed = require('./breed');
var Class = require('./class');

function Character(name, classname, breedname) {
	this.name = name;
	this.class = new Class(classname);
	this.breed = new Breed(breedname);
	this.toString = function () { return this.name + ', ' + this.breed + ' ' + this.class; };
};

module.exports = Character;