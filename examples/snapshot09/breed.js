define([], function () {
	return function Breed(breedname) {
		this.name = breedname;
		this.toString = function () { return this.name; };
	};
});