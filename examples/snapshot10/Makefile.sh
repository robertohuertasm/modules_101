#!/bin/bash

# clean generated files
rm -f main-bundle*

# Depends on having r.js command installed, with the command:
# npm install requirejs -g
#main.js -> main-bundle.js using build.js config file
r.js -o build.js

# Depends on having uglifyjs command installed, with the command:
# npm install uglifyjs -g
uglifyjs main-bundle.js --compress --mangle -o main-bundle-min.js
