#!/bin/bash

# clean generated files
rm -f main-bundle.js

# Depends on having browserify command installed, with the command:
# npm install browserify -g

# Also depends on having babelify installed locally
# npm install babelify
browserify -t babelify main.js -o main-bundle.js