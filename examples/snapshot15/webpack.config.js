/// <reference path="../../typings/node/node.d.ts"/>
module.exports = {
    module: {
        loaders: [
            { test: /\.js$/ ,
			  exclude: /node_modules/,
              loader: 'babel-loader' }
        ]
    }
};