requirejs.config({
	config: {
		es6: {
			resolveModuleSource: function (source) {
				return 'es6!' + source;
			}
		}
	},
	paths: {
		es6: 'node_modules/requirejs-babel/es6',
		babel: 'node_modules/requirejs-babel/babel-4.6.6.min'
	}
});
require(['es6!./main']);