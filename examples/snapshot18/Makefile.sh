#!/bin/bash

# clean generated files
rm -f main-bundle.js

# Depends on having esperanto command installed, with the command:
# npm install esperanto -g
esperanto --type umd --name main --input main.js --output main-bundle.js --bundle