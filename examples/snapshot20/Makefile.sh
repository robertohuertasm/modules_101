#!/bin/bash

# clean generated files
rm -f main-bundle.js
rm -f main-bundle.js.map

# Depends on having jspm command installed, with the command:
# npm install jspm -g
jspm bundle-sfx main main-bundle.js
