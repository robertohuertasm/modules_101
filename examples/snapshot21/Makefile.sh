#!/bin/bash

# clean generated files
rm -f main-bundle.js

# Depends on having jspm command installed, with the command:
# npm install jspm -g
jspm bundle-sfx main main-bundle.js
