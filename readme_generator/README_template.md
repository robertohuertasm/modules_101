# Modules 101

* [Slides and Code Examples](http://tokiotajs.bitbucket.org/modules101/viewer/)

## Examples

Check out the [Example Viewer](http://tokiotajs.bitbucket.org/modules101/viewer/). Use left and right arrow keys to navigate.

<%= examples %>



  ```
  ```
___
By [Sílvia Mur](https://twitter.com/pchiwan) & [Roberto Huertas](https://twitter.com/newton_w).   
June 2015

Feel free to use and modify this presentation framework for your own presentations! MIT License.  
Based on [Curran Kelleher's amazing work!](https://github.com/curran/portfolio)   

